## Data Reseller App

## Requirement 
This application requires PHP Version >=7.0

### Installation 
1. Clone the repository ``` git clone https://github.com/saviobosco/dataresellerapp.git ```
2. Ensure you have composer installed in your machine and added to the ENVIRONMENT_PATH else if you don't have composer, you can download it [here](https://getcomposer.org/). 
3. Open your terminal and navigate into the data-reseller-app root directory
4. To install the dependencies run ``` composer install ```
4. You need to create a database.sqlite file, run ``` touch database/database.sqlite ```
5. To execute the migration, run ``` php artisan migrate ```
6. Then, start the application by executing ```php artisan serve``` and open your browser and navigate to ```localhost:8000``` to access the application

## License

This application is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
