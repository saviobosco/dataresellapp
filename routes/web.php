<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function() {
    Route::get('/home', 'HomeController@index')->name('home');

    // Network
    Route::get('/networks', 'NetworksController@index')->name('networks');
    Route::get('/networks/create', 'NetworksController@create')->name('networks.create');
    Route::post('/networks/create', 'NetworksController@store')->name('networks.create.post');
    Route::get('/networks/edit/{network}', 'NetworksController@edit')->name('networks.edit');
    Route::put('/networks/edit/{network}', 'NetworksController@update')->name('networks.edit.put');
    Route::get('/networks/delete/{network}', 'NetworksController@delete')->name('networks.delete');
    Route::delete('/networks/delete/{network}', 'NetworksController@destroy')->name('networks.delete.delete');

    // Pin
    Route::get('/pins', 'PinsController@index')->name('pins');
    Route::get('/pins/create', 'PinsController@create')->name('pins.create');
    Route::post('/pins/create', 'PinsController@store')->name('pins.create.post');
    Route::get('/pins/edit/{pin}', 'PinsController@edit')->name('pins.edit');
    Route::put('/pins/edit/{pin}', 'PinsController@update')->name('pins.edit.put');
    Route::get('/pins/delete/{pin}', 'PinsController@delete')->name('pins.delete');
    Route::delete('/pins/delete/{pin}', 'PinsController@destroy')->name('pins.delete.delete');

    Route::post('/pins/checkPin', 'CheckPinController@checkPin')->name('pins.checkPin');

    Route::get('/pins/export_excel', 'ExportExcelController@exportExcel')->name('pins.export_excel');
    Route::post('/pins/export_excel', 'ExportExcelController@processExportExcel')->name('pins.export_excel.post');
});

