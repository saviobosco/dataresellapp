<?php

namespace App\Http\Controllers;

use App\Exports\PinsExport;
use App\Pin;
use Illuminate\Http\Request;

class ExportExcelController extends Controller
{
    public function exportExcel()
    {
        return view('pins.export_excel');
    }

    public function processExportExcel(Request $request)
    {
        $this->validate($request, [
            'date' => 'required'
        ]);
        return (new PinsExport($request->input('date')))->download('pin.xlsx');
    }
}
