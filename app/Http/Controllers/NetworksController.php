<?php

namespace App\Http\Controllers;

use App\Network;
use Illuminate\Http\Request;

class NetworksController extends Controller
{
    public function index()
    {
        $networks = Network::all();
        return view('networks.index')->with(compact('networks'));
    }

    public function create()
    {
        return view('networks.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        if (Network::create($request->only(['name']))) {
            flash('Network has been created')->success();
        }
        return back();
    }

    public function edit(Network $network)
    {
        return view('networks.edit')->with(compact('network'));
    }

    public function update(Network $network, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        if ($network->update($request->only(['name']))) {
            flash('Network has been updated!')->success();
        }
        return back();
    }

    public function delete(Network $network)
    {
        return view('networks.delete')->with(compact('network'));
    }

    public function destroy(Network $network, Request $request)
    {
        if ($request->isMethod('delete')) {
            if ($network->delete()) {
                flash('network has been deleted!')->success();
                return redirect('networks');
            }
        }
        return back();
    }
}
