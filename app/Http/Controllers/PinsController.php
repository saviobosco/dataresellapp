<?php

namespace App\Http\Controllers;

use App\Network;
use App\Pin;
use Illuminate\Http\Request;
use RandomLib\Factory;
use SecurityLib\Strength;

class PinsController extends Controller
{
    public function index()
    {
        $pins = Pin::query()->with(['network'])->orderBy('used')->get()->toArray();
        return view('pins.index')->with(compact('pins'));
    }

    public function create()
    {
        $networks = Network::pluck('name', 'id');
        return view('pins.create')->with(compact('networks'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'network_id' => 'required',
            'value' => 'required|integer',
            'number_to_generate' => 'required|integer'
        ]);
        $factory = new Factory;
        $generator = $factory->getGenerator(new Strength(Strength::MEDIUM));
        $pins = null;
        for($num = 0; $num < $request->input('number_to_generate'); $num++) {
            Pin::create([
                'value' => $request->input('value'),
                'pin' => $generator->generateInt(1111111, 9999999),
                'network_id' => $request->input('network_id')
            ]);
        }
        flash($request->input('number_to_generate') . ' was successfully generated ');
        return back();
    }

    public function edit(Pin $pin)
    {
        $networks = Network::pluck('name', 'id');
        return view('pins.edit')->with(compact('pin', 'networks'));
    }

    public function update(Request $request, Pin $pin)
    {
        $pin->fill($request->only(['used_by']));
        if ((int)$request->input('used_at') === 1) {
            $pin->fill(['used_at' => now()]);
        }
        if ($pin->update()) {
            flash('Pin details was successfully updated')->success();
        } else {
            flash('Error updating pin details')->error();
        }
        return back();
    }

    public function delete(Pin $pin)
    {
        return view('pins.delete')->with(compact('pin'));
    }

    public function destroy(Request $request, Pin $pin)
    {
        if ($request->isMethod('delete')) {
            if ($pin->delete()) {
                flash('pin has been deleted!')->success();
                return redirect('pins');
            }
        }
        return back();
    }
}
