<?php

namespace App\Http\Controllers;

use App\Pin;
use Illuminate\Http\Request;

class CheckPinController extends Controller
{
    public function checkPin(Request $request)
    {
        $pin = Pin::query()->where('pin', $request->input('pin'))->orWhere('id', $request->input('serial_number'))->first();
        if (is_null($pin)) {
            flash('Pin does not exists')->error();
        } else {
            return redirect('/pins/edit/'. $pin->id);
        }
        return back();
    }
}
