<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    protected $guarded = ['id'];

    public function pins()
    {
        return $this->hasMany(Pin::class);
    }
}
