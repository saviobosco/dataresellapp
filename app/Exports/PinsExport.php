<?php

namespace App\Exports;

use App\Pin;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PinsExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    protected $date;

    public function __construct( $date)
    {
        $this->date = $date;
    }

    public function query()
    {
        return Pin::query()->with(['network:id,name'])->whereDate('created_at', $this->date);
    }

    public function map($pin): array
    {
        return [
            $pin->id,
            $pin->pin,
            $pin->network->name,
            $pin->value,
            $pin->created_at
        ];
    }

    public function headings(): array
    {
        return [
            'Serial Number',
            'Pin',
            'Network',
            'Value',
            'Date Created'
        ];
    }
}
