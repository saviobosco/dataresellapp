@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="panel panel-heading">
                    <div class="panel-heading">{{ __('Generate New Pins') }}</div>

                    <div class="panel-body">
                        <form method="POST" action="{{ route('pins.create.post') }}">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Select Network') }}</label>

                                <div class="col-md-6">
                                    <select id="network" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="network_id" required>
                                        <option/>
                                        @foreach($networks as $id => $name)
                                            <option value="{{ $id }}"> {{ $name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="value" class="col-sm-4 col-form-label text-md-right">{{ __('Value') }}</label>

                                <div class="col-md-6">
                                    <input id="value" type="number" class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" value="{{ old('value') }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="number_to_generate" class="col-sm-4 col-form-label text-md-right">{{ __('Number To Generate') }}</label>

                                <div class="col-md-6">
                                    <input id="number_to_generate" type="number" class="form-control{{ $errors->has('number_to_generate') ? ' is-invalid' : '' }}" name="number_to_generate" value="{{ old('number_to_generate') }}" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
