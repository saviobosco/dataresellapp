@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="panel panel-heading">
                    <div class="panel-heading">{{ __('Update Pin Details') }}</div>

                    <div class="panel-body">
                        <form method="POST" action="{{ route('pins.edit.put', ['pin' => $pin->id]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Select Network') }}</label>

                                <div class="col-md-6">
                                    <select id="network" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="network_id" disabled>
                                        @foreach($networks as $id => $name)
                                            <option value="{{ $id }}"
                                                    {{ ((int)$id === (int)$pin->network_id) ? 'selected' : '' }}
                                                    > {{ $name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="value" class="col-sm-4 col-form-label text-md-right">{{ __('Value') }}</label>

                                <div class="col-md-6">
                                    <input id="value" type="number" class="form-control" name="value" value="{{ $pin->value }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="used_by" class="col-sm-4 col-form-label text-md-right">{{ __('Used By') }}</label>

                                <div class="col-md-6">
                                    <input id="used_by" type="text" class="form-control" name="used_by" value="{{ $pin->used_by }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="used_at" class="col-sm-4 col-form-label text-md-right">{{ __('Used') }}</label>


                                <div class="col-sm-6">
                                    <input type="hidden" name="used_at" value="0">
                                    <label for="used_at">
                                        <input type="checkbox" name="used_at" id="used_at" value="1" <?= ($pin->used_at) ? 'checked="checked"' : '' ?> >
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
