@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="pull-right" href="{{ url('/pins/create') }}"> Generate Pins </a>
                        <a style="margin-right:10px" class="pull-right" href="{{ url('/pins/export_excel') }}"> Export Pins </a>
                        All Networks  </div>

                    <div class="panel-body">
                        <table class="table table-responsive-sm">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th> network </th>
                                <th> pin </th>
                                <th> value </th>
                                <th> used at </th>
                                <th> used by </th>
                                <th> created </th>
                                <th> modified </th>
                                <th> actions </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pins as $pin)
                                <tr>
                                    <td> {{ $pin['id'] }} </td>
                                    <td> {{ $pin['network']['name'] }} </td>
                                    <td> {{ $pin['pin'] }} </td>
                                    <td> &#8358;{{ $pin['value'] }} </td>
                                    <td> {{ $pin['used_at'] }} </td>
                                    <td> {{ $pin['used_by'] }} </td>
                                    <td> {{ $pin['created_at'] }} </td>
                                    <td> {{ $pin['updated_at'] }} </td>
                                    <td>
                                        <a href="{{ route('pins.edit', ['pin' => $pin['id']]) }}"> edit </a> |
                                        <a href="{{ route('pins.delete', ['pin' => $pin['id']]) }}"> delete </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
