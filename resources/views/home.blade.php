@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading"> Check Pin</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <form method="POST" action="{{ route('pins.checkPin') }}">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="pin" class="col-sm-4 col-form-label text-md-right">{{ __('Pin') }}</label>

                                <div class="col-md-6">
                                    <input id="pin" type="number" class="form-control{{ $errors->has('pin') ? ' is-invalid' : '' }}" name="pin" value="{{ old('pin') }}">
                                </div>
                            </div>
                            <p> OR </p>

                            <div class="form-group row">
                                <label for="serial_number" class="col-sm-4 col-form-label text-md-right">{{ __('Serial Number') }}</label>

                                <div class="col-md-6">
                                    <input id="serial_number" type="number" class="form-control{{ $errors->has('serial_number') ? ' is-invalid' : '' }}" name="serial_number" value="{{ old('serial_number') }}">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Check Pin') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading"> Activities </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
