@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-heading">{{ __('Delete Network') }}</div>

                    <div class="panel-body">
                        <form method="POST" action="{{ route('networks.delete.delete', ['network' => $network->id]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">

                            <h4> Are you sure you want to delete {{ $network->name }}</h4>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <a class="btn btn-primary" href="{{ route('networks') }}"> Go back </a>
                                    <button type="submit" class="btn btn-danger">
                                        {{ __('Delete') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
