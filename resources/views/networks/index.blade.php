@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading"> <a class="pull-right" href="{{ url('/networks/create') }}"> New Network </a>  All Networks  </div>

                    <div class="panel-body">
                        <table class="table table-responsive-sm">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th> network name</th>
                                <th> actions </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($networks as $network)
                                <tr>
                                    <td> {{ $network['id'] }} </td>
                                    <td> {{ $network['name'] }} </td>
                                    <td>
                                        <a href="{{ route('networks.edit', ['network' => $network['id']]) }}"> edit </a> |
                                        <a href="{{ route('networks.delete', ['network' => $network['id']]) }}"> delete </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
